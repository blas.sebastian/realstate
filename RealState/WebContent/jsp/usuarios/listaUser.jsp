<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Usuarios</title>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</head>
<body style="background-color: #dedede">
	<%@ include file="../componentes/navbar.jsp" %>
	<div class="container">
	  <h1 class="text-dark mt-3 text-left">Mantenedor de Usuarios</h1>	  	 
	  
	  <form class="form-inline mt-3 justify-content-end" >
		  <input class="form-control mr-sm-2" type="search" placeholder="Rut" aria-label="Search">
      	  <button class="btn btn-dark my-2 my-sm-0" type="submit">Buscar</button>
	  </form>	  
	
	<table class="shadow p-3 mb-5 bg-white rounded table table-hover table-light table-bordered mt-4" >
	  <thead>
	    <tr>
	      <th scope="col">#</th>
	      <th scope="col">Rut</th>
	      <th scope="col">Nombre</th>
	      <th scope="col">Tipo Usuario</th>
	      <th scope="col">Celular</th>
	      <th scope="col">Estado</th>
	    </tr>
	  </thead>
	  <tbody>
	    <tr>
	      <th scope="row"><button type="button" class="btn btn-link">>></button></th>
	      <td>19289384-k</td>
	      <td>Juan Chavez</td>
	      <td>Agente</td>
	      <td>987654321</td>
	      <td>Activo</td>
	    </tr>
	    <tr>
	      <th scope="row"><button type="button" class="btn btn-link">>></button></th>
	      <td>19289384-k</td>
	      <td>Javiera Cortez</td>
	      <td>Usuario</td>
	      <td>987654321</td>
	      <td>Inactivo</td>
	    </tr>
	    <tr>
	      <th scope="row"><button type="button" class="btn btn-link">>></button></th>
	      <td>19289384-k</td>
	      <td>Daniela Jerez</td>
	      <td>Administrador</td>
	      <td>987654321</td>
	      <td>Activo</td>
	    </tr>
	  </tbody>
	</table>
	<form action="../index.jsp" class="mt-8">
		<button class="btn btn-dark btn-block" >Volver</button>
		<input type="hidden" value="2" name="login">
	</form>
	</div>
	
	
	
</body>
</html>