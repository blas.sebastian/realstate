<nav class="navbar-expand-lg navbar navbar-dark " style="background-color: rgb(113, 72, 60);" >
	  
	  <a class="navbar-brand" href="#">Real State</a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	
	  <div class="collapse navbar-collapse" id="navbarSupportedContent">
	    <ul class="navbar-nav ml-auto mr-auto">
	      <li class="nav-item active">
	        <a class="nav-link btn-lg mr-3" href="#">Inicio</a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link btn-lg mr-3" href="#">Quienes Somos</a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link btn-lg mr-3" href="#">Propiedades</a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link btn-lg mr-3" href="#">Nuestro Equipo</a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link btn-lg mr-3" href="#">Contacto</a>
	      </li>
	      <%if(!request.getParameter("login").equals("1")) {%>
	      <li class="nav-item dropdown">
	        <a class="nav-link dropdown-toggle btn-lg" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	          Administrador
	        </a>
	        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
	          <a class="dropdown-item" href="/RealState/jsp/usuarios/listaUser.jsp?login=2">Usuarios</a>
	          <a class="dropdown-item" href="/RealState/jsp/propiedades/listaPropiedades.jsp?login=2">Propiedades</a>
	          <a class="dropdown-item" href="#">Agenda</a>	          
	        </div>
	      </li>
	      <%} %>	      	     	   	      	     	      	      
	    </ul>	    
	    <%if(request.getParameter("login").equals("1")) {%>
	    <form class="form-inline my-2 my-lg-0" action="/RealState/jsp/login.jsp">      	      
	      	<button class="btn btn-danger" type="submit">Login/Registro</button>	      
	    </form>
	    <%}else{%>
	    <form class="form-inline my-2 my-lg-0" action="/RealState/jsp/index.jsp">
	      	<button class="btn btn-danger" type="submit">Salir</button>
	      	<input type="hidden" value="1" name="login">
	      	<%} %>
      	</form>
	  </div>
	</nav>