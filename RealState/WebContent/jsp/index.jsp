<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Real State</title>
<link rel="stylesheet" href="../css/style.css" >
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</head>
<body>	
	<%@ include file="componentes/navbar.jsp" %>	
	<div class="container card-columns">
	  <div class="card">
	    <img class="card-img-top" src="../img/casa1.jpg" alt="Card image cap">
	    <div class="card-body">
	      <h5 class="card-title">Card title that wraps to a new line</h5>
	      <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
	    </div>
	  </div>
	  <div class="card p-3">
	    <blockquote class="blockquote mb-0 card-body">
	      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
	      <footer class="blockquote-footer">
	        <small class="text-muted">
	          Someone famous in <cite title="Source Title">Source Title</cite>
	        </small>
	      </footer>
	    </blockquote>
	  </div>
	  <div class="card">
	    <img class="card-img-top" src="../img/casa2.jpg" alt="Card image cap">
	    <div class="card-body">
	      <h5 class="card-title">Card title</h5>
	      <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
	      <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
	    </div>
	  </div>
	  <div class="card bg-primary text-white text-center p-3">
	    <blockquote class="blockquote mb-0">
	      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat.</p>
	      <footer class="blockquote-footer">
	        <small>
	          Someone famous in <cite title="Source Title">Source Title</cite>
	        </small>
	      </footer>
	    </blockquote>
	  </div>
	  <div class="card text-center">
	    <div class="card-body">
	      <h5 class="card-title">Card title</h5>
	      <p class="card-text">This card has a regular title and short paragraphy of text below it.</p>
	      <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
	    </div>
	  </div>
	  <div class="card">
	    <img class="card-img" src="../img/casa3.jpg" alt="Card image">
	  </div>
	  <div class="card p-3 text-right">
	    <blockquote class="blockquote mb-0">
	      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
	      <footer class="blockquote-footer">
	        <small class="text-muted">
	          Someone famous in <cite title="Source Title">Source Title</cite>
	        </small>
	      </footer>
	    </blockquote>
	  </div>
	  <div class="card">
	    <div class="card-body">
	      <h5 class="card-title">Card title</h5>
	      <p class="card-text">This is another card with title and supporting text below. This card has some additional content to make it slightly taller overall.</p>
	      <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
	    </div>
	  </div>
	</div>		
</body>
</html>